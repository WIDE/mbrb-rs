# Presentation
This repository presents a Rust implementation of the message-adversary-tolerant Byzantine reliable broadcast (MBRB) distributed algorithm described in [*Byzantine-Tolerant Reliable Broadcast in the Presence of Silent Churn*][sb-mbrb] (Albouy, Frey, Raynal, Taïani, SSS 2021, [full version][sb-mbrb-full]).
To achieve optimal resilience and performance, this implementation leverages digital signatures (more specifically, BLS multisignatures to perform signature aggregation).
This implementation allows you to launch one or several MBRB instances, and make them communicate by disseminating (mbrb-broadcast) and receiving (mbrb-deliver) messages.

# Building
Compiling the code requires having the Rust compiler and Cargo installed: [rust-lang.org/tools/install](https://www.rust-lang.org/tools/install).

To build the project from the root directory: `cargo build`.

# Experiment folder
All the useful resource files for conducting experiments are stored in the `expe/` folder.

## Keys folder
The `expe/keys/` folder contains the secret and public keys (in the `sec/` and `pub/` subfolders, respectively) used for the experiments.

## Configuration folder
The `expe/config/` folder contains 3 types of configuration files in the JSON format: global configs, run configs and process configs (in the `global/`, `run/` and `proc/` (not tracked) subfolders, respectively).

### Global configs
Global configs store the information on experimental configurations that can be reused for several executions (or runs) of an experiment.
Here are the JSON fields:
- `name` (string): name (identifier) of the global configuration,
- `n` (positive int): total number of processes,
- `t` (positive int): maximum number of Byzantine processes,
- `d` (positive int): maximum number of messages that can be suppressed at each ur-broadcast,
- `transport` (string): transport protocol used, either `"UDP"` or `"TCP"`,
- `usedCities` (array of strings): list of cities that are used for virtual latencies (only used in Grid'5000 experiments)
- `actions` (object string => array): map containing all the actions that must be performed by some processes of the network.
The key is either `"before"`, `"after"` (if some actions must be performed by all processes of the network, before or after everything else, respectively) or the process identifier (if some actions must be performed by specific processes).
The value is an array containing all actions to be performed by the selected processes.
An action is an array containing in the first place the type of actions, and in the remaining places the required data for this action.
Currently, the following actions are implemented:
  - `["mbrb", msg, sn]`: the selected process must mbrb-broadcast `msg` with sequence number `sn`,
  - `["wait", dur_sec]`: the selected process must wait `dur_sec` seconds.

### Run configs
Run configs store the information for one single execution (or run) of an experiment that follows a given global config.
Here are the JSON fields:
- `timestamp` (string): timestamp (following ISO 8601) of the current run of the experiment,
- `globalConfig` (string): name of the global config that is used,
- `parentLogFolderPath` (string): parent folder of the log folder to be used,
- `processes` (array): array containing the connection information for each of the processes of the experiment run.
Each process connection information is a JSON object structured this way:
  - `procId` (positive int): identifier of the process,
  - `addr` (string): IP address (V4 or V6) or hostname of the process,
  - `port` (positive int): transport layer port of the process.

### Node configs
Node configs store the process id and  both the global and run configs, but with the process id. 
Here are the JSON fields:
- `city` (string): name of the city emulated by the node for virtual latency injection (only used in Grid'5000 experiments),
- `runTimestamp` (string): timestamp of the corresponding run config,
- `addr` (string): IP address (V4 or V6) or hostname of the node.

## Log folder
The `expe/logs/` folder (not tracked) keeps the logs of all processes for each experiment run in a separate subfolder.
The name of each subfolder follows the pattern `expe-<timestamp of the run config>`.
Currently, only mbrb-broadcasts, mbrb-deliveries, network message ur-broadcasts and receptions are logged.

## Results folder
TODO

# Conducting experiments
The `scripts/` folder contains Jupyter Notebooks and Python scripts for conducting the experiments (on Grid'5000 or locally), analyzing the results, producing the intermediate data files, and producing graphs.

## Step 1: Run experiments

### Option 1: Local experiments
Before running experiments on a full-fledged computing grid, it is useful to test the implementation locally.
To manually launch a single process, you can run from the root directory `cargo run -- expe <global config path> <run config path> <process id> &`:
- `global config path`: the path of the global config JSON file,
- `run config path`: the path of the run config JSON file,
- `process id`: the ID of the process to launch.

For launching all the processes of a node configuration, you can run from the root directory `python scripts/run_node_processes.py <node config path>`:
- `node config path`: the path of the node config JSON file.

## Option 2: Grid'5000 experiments
To run experiments on Grid'5000, you must first create an account on [grid5000.fr](https://www.grid5000.fr/) and register your personal SSH public key.
To simplify the connection, you can add the following entries in your personal `~/.ssh/config` file:

```
Host g5k
  User <your username>
  Hostname access.grid5000.fr
  ForwardAgent no

Host *.g5k
  User <your username>
  ProxyCommand ssh g5k -W "$(basename %h .g5k):%p"
  ForwardAgent no
  
Host !access.grid5000.fr *.grid5000.fr
   User <your username>
   ProxyJump <your username>@access.grid5000.fr
   StrictHostKeyChecking no
   UserKnownHostsFile /dev/null
   ForwardAgent yes
```

The Jupyter Notebook `expe_mbrb_step_1_g5k.ipynb` contains all the steps for running experiments on the Grid'5000 computing grid and fetching the resulting logs from all the nodes of the experiment.
To change the global config, the Grid'5000 username or any other value for the experiment, edit the "Define constants" cell of the notebook.
It is often necessary to manually connect via SSH to every Grid'5000 site prior to running the experiments, in order to add all of the sites' SSH public keys to the trusted endpoints.

### Step 2: Analyze the logs
Once the logs have been fetched from the experiment nodes (or produced locally), we can extract the data from the logs using the `expe_mbrb_step_3_analysis.py` to produce a single JSON file agregating all relevant information to produce graphs in the final step.
This JSON file is stored in the `expe/results/` folder.

⚠️ In order to parse ISO 8601 dates from log files with more than 6 decimals for fractional seconds, the script must be executed using Python 3.11 or above ([source][iso-date-6-decimals]).

#### Step3: Produce graphs
TODO

## The MBRB abstraction
MBRB is a kind of reliable broadcast that functions in a network of $n$ message-passing processes that is prone to Byzantine faults and messages losses (caused by a message adversary).
In such an environment, the variable $t$ denotes the maximum number of processes in the network that can commit to Byzantine faults (arbitrary behaviours), and the variable $d$ denotes the maximum number of network messages sent by a process during one communication step that the message adversary can suppress.
Furthermore, we also denote by the variable $c$ the effective number of correct (i.e. non-Byzantine) processes in the network, such that $n-t \leq c \leq n$.
This implementation guarantees the following specification, provided that the condition $n > 3t+2d$ is satisfied (tight condition).
For proofs, please refer to the above article.

**Safety:**
- *MBRB-Validity*: If a correct process $p_i$ mbrb-delivers a message $m$ from a correct process $p_j$ with sequence number $\mathit{sn}$, then $p_j$ mbrb-broadcast $m$ with sequence number $\mathit{sn}$.
- *MBRB-No-duplication*: A correct process $p_i$ mbrb-delivers at most one message $m$ from a process $p_j$ with sequence number $\mathit{sn}$.
- *MBRB-No-duplicity*: No two different correct processes mbrb-deliver different messages from a process $p_i$ with the same sequence number $\mathit{sn}$.

**Liveness:**
- *MBRB-Local-delivery*: If a correct process $p_i$ mbrb-broadcasts a message $m$ with sequence number $\mathit{sn}$, then at least one correct process $p_j$ eventually mbrb-delivers $m$ from $p_i$ with sequence number $\mathit{sn}$.
- *MBRB-Global-delivery*: If a correct process $p_i$ mbrb-delivers a message $m$ from a process $p_j$ with sequence number $\mathit{sn}$, then at least $\ell$ correct processes mbrb-deliver $m$ from $p_j$ with sequence number $\mathit{sn}$.

As there is a message adversary that can suppress up to $d$ network messages sent by every correct processes at each communication step, then the message adversary can totally isolate up to $d$ correct processes such that they would not receive any network message.
It entails that, unlike in the traditional BRB specification, we cannot guarantee that all $c$ correct processes eventually deliver the message $m$, but that at most $c-d$ correct processes do so.
The variable $\ell$ denotes the number of deliveries by correct processes of the MBRB-Global-delivery property that the given implementation guarantees.
An implementation with good guarantees provides a high $\ell$, whereas one with bad guarantees provides a low $\ell$.
The present implementation provides $\ell=c-d$ (the best possible guarantee, as seen before).
Let us further remark that when we have $\ell=c$ (which can only be obtained when $d=0$, that is when there is no message adversary), then the previous specification boils down to classical BRB, hence MBRB generalized BRB.


[sb-mbrb]: https://link.springer.com/chapter/10.1007/978-3-030-91081-5_2
[sb-mbrb-full]: https://arxiv.org/abs/2205.09992
[iso-date-6-decimals]: https://github.com/python/cpython/issues/95221