pub trait KeyGen<S, P> where S: SecKey, P: PubKey {
	fn generate(&self) -> (S, P);

	fn sec_key_from_bytes(&self, b: &[u8]) -> S;

	fn pub_key_from_bytes(&self, b: &[u8]) -> P;
}

pub trait Key {
	fn to_bytes(&self) -> Vec<u8>;
}

pub trait SecKey: Key + Send {
	fn sign(&self, msg: &[u8]) -> Vec<u8>;
}

pub trait PubKey: Key + Send + Clone {
	fn verify(&self, msg: &[u8], sig: &[u8]) -> bool;
}

pub trait MultiSigHandler<P>: Send where P: PubKey {
	fn aggregate(&self, sigs: &[&[u8]]) -> Result<Vec<u8>, String>;

	fn verify(&self, pub_keys: &[P], msg: &[u8], multisig: &[u8]) -> bool;
}
