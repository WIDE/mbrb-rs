//use blst::min_pk::*;		// for minimal-pubkey-size operations
use blst::min_sig::*;		// for minimal-signature-size operations
use rand_core::{OsRng, RngCore};

use super::iface::{KeyGen, SecKey, PubKey, Key, MultiSigHandler};

pub struct BLSKeyGen;

impl KeyGen<BLSSecKey, BLSPubKey> for BLSKeyGen {
	fn generate(&self) -> (BLSSecKey, BLSPubKey) {
		let mut ikm = [0u8; 96];
		OsRng.fill_bytes(&mut ikm);
		let sig_key = SecretKey::key_gen(&ikm, &[]).unwrap();
		let pub_key = BLSPubKey { pub_key: sig_key.sk_to_pk() };
		let priv_key = BLSSecKey { sec_key: sig_key };
		(priv_key, pub_key)
	}

	fn sec_key_from_bytes(&self, b: &[u8]) -> BLSSecKey {
		BLSSecKey { sec_key: SecretKey::from_bytes(b).unwrap() }
	}

	fn pub_key_from_bytes(&self, b: &[u8]) -> BLSPubKey {
		BLSPubKey { pub_key: PublicKey::from_bytes(b).unwrap() }
	}
}

const SIG_DST: &[u8] = b"BLS_SIG_BLS12381G2_XMD:SHA-256_SSWU_RO_NUL_";

pub struct BLSSecKey {
	sec_key: SecretKey
}

impl SecKey for BLSSecKey {
	fn sign(&self, msg: &[u8]) -> Vec<u8> {
		self.sec_key.sign(msg, SIG_DST, &[]).to_bytes().to_vec()
	}
}

impl Key for BLSSecKey {
	fn to_bytes(&self) -> Vec<u8> {
		self.sec_key.to_bytes().to_vec()
	}
}

#[derive(Clone)]
pub struct BLSPubKey {
	pub_key: PublicKey
}

impl PubKey for BLSPubKey {
	fn verify(&self, msg: &[u8], sig: &[u8]) -> bool {
		match Signature::from_bytes(sig) {
			Ok(sig) => match sig.verify(false, msg, &SIG_DST, &[], &self.pub_key, false) {
				blst::BLST_ERROR::BLST_SUCCESS => true,
				_ => false
			},
			Err(_) => false
		}
	}
}

impl Key for BLSPubKey {
	fn to_bytes(&self) -> Vec<u8> {
		self.pub_key.to_bytes().to_vec()
	}
}

pub struct BLSMultiSigHandler;

impl MultiSigHandler<BLSPubKey> for BLSMultiSigHandler {
	fn aggregate(&self, sigs: &[&[u8]]) -> Result<Vec<u8>, String> {
		let mut sigs_vec = vec![];
		for sig in sigs {
			match Signature::from_bytes(sig) {
				Ok(sig) => sigs_vec.push(sig),
				Err(_) => return Err(String::from("Some signatures cannot be decoded from bytes."))
			}
		}
		let sigs_refs_vec: Vec<&Signature> = sigs_vec.iter().collect();
		match AggregateSignature::aggregate(&sigs_refs_vec, false) {
			Ok(multisig) => Ok(multisig.to_signature().to_bytes().to_vec()),
			Err(_) => Err(String::from("Signatures cannot be aggregated."))
		}
	}

	fn verify(&self, pub_keys: &[BLSPubKey], msg: &[u8], multisig: &[u8]) -> bool {
		let mut pub_keys_vec = vec![];
		for pub_key in pub_keys {
			pub_keys_vec.push(pub_key.pub_key);
		}
		let pub_keys_refs_vec: Vec<&PublicKey> = pub_keys_vec.iter().collect();
		match Signature::from_bytes(multisig) {
			Ok(multisig) => match multisig.fast_aggregate_verify(false, msg, SIG_DST, &pub_keys_refs_vec) {
				blst::BLST_ERROR::BLST_SUCCESS => true,
				_ => false
			},
			Err(_) => false
		}
    }
}
