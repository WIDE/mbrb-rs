pub const PROC_ID_N_BYTES: usize = 2; // processes IDs take up 16 bits
pub const SN_N_BYTES: usize = 1; // sequence numbers take up 8 bits

pub trait MBRB {
	fn mbrb_broadcast(&mut self, msg: &[u8], sn: u8) -> Result<(), String>;
}
