use std::{
	collections::{VecDeque, HashMap},
	sync::Arc
};

use super::{
	super::iface::{SN_N_BYTES, PROC_ID_N_BYTES},
	payload::*
};

pub struct BundleMultiSig {
	pub payload: Payload,
	pub multisig: Arc<Vec<u8>>,
	pub signer_ids_reps: Arc<HashMap<u16, u16>>
}

/*
BUNDLE MARSHALLING:
+---------------------------------+
|               MSG               |
+----------------+----------------+
|       SN       |     SND_ID     |
+----------------+----------------+
|  MULTISIG_LEN  |    MULTISIG    |   
+----------------+----------------+
|    N_SIGNERS   |                |
+----------------+     SIGNER     |
|                      COUPLES    |
|                                 |
+---------------------------------+

SIGNER COUPLE MARSHALLING:
+----------------+----------------+
|     PROC_ID    |      REPS      |
+----------------+----------------+
*/

impl BundleMultiSig {
	pub fn to_bytes(&self) -> Vec<u8> {
		let mut bytes = self.payload.to_bytes();

		let multisig_len = self.multisig.len() as u8;
		let mut multisig = (*self.multisig).clone();
		bytes.push(multisig_len);
		bytes.append(&mut multisig);

		// there are at most as many signatures as there are processes in the system
		let n_sigs = self.signer_ids_reps.len() as u16;
		let (v1, v2) = u16_to_two_u8(n_sigs);
		bytes.push(v1);
		bytes.push(v2);
		
		for (&signer_id, &reps) in self.signer_ids_reps.iter() {
			let (v1, v2) = u16_to_two_u8(signer_id);
			bytes.push(v1);
			bytes.push(v2);
			let (v1, v2) = u16_to_two_u8(reps);
			bytes.push(v1);
			bytes.push(v2);
		}

		bytes
	}

	pub fn from_bytes(bytes: &[u8]) -> Option<BundleMultiSig> {
		let mut bytes = VecDeque::from(bytes.to_vec());

		// if there are not enough bytes for the beginning fixed-size fields of the byte sequence
		if bytes.len() < MBRB_MSG_N_BYTES+SN_N_BYTES+2*PROC_ID_N_BYTES {
			return None;
		}

		// extract payload
		let msg = Arc::new(bytes.drain(0..MBRB_MSG_N_BYTES).collect());
		let sn = bytes.pop_front().unwrap();
		let sender_id = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
		let payload = Payload { msg, sn, sender_id };
		
		// extract multisignature
		let multisig_len = bytes.pop_front().unwrap();
		// if there are not enough bytes for the multisignature and the number of signers
		if bytes.len() < usize::from(multisig_len)+PROC_ID_N_BYTES {
			return None;
		}
		let mut multisig = vec![];
		for _ in 0..multisig_len {
			multisig.push(bytes.pop_front().unwrap());
		}
		
		// extract signers
		let n_signers = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
		// if there are not enough bytes for the multisignature and the number of signers
		if bytes.len() < usize::from(n_signers)*PROC_ID_N_BYTES*2 {
			return None;
		}
		let mut signer_ids_reps = HashMap::new();
		for _ in 0..n_signers {
			let signer_id = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
			let reps = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
			signer_ids_reps.insert(signer_id, reps);
		}

		Some(BundleMultiSig { payload, multisig: Arc::new(multisig), signer_ids_reps: Arc::new(signer_ids_reps) })
	}
}
