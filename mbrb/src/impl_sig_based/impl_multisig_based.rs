use std::{
	collections::{HashMap, HashSet},
	sync::{Arc, Mutex}
};

use log::{info, warn};

use net::{
	network::Network,
	msg_adversary::iface::MsgAdversary
};
use crypto::iface::*;
use super::{
	super::iface::*,
	payload::*,
	bundle_multisig::*
};

pub struct MultiSigBasedMBRB<S, P, M, H>
	where S: SecKey, P: PubKey, M: MsgAdversary, H: MultiSigHandler<P>
{
	network: Network<M>,
	t: u16,
	deliver_handler: Arc<dyn Fn(Vec<u8>, u8, u16) + Send + Sync>, // (msg, sn, sender_id) -> ()
	proc_id: u16,
	sec_key: S,
	pub_keys: HashMap<u16, P>,
	multisig_handler: H,
	delivered_msgs: HashSet<(u8, u16)>, // { (sn, sender_id) }
	saved_multisigs: HashMap<
		(u8, u16),
		HashMap<
			Arc<Vec<u8>>,
			(Arc<Vec<u8>>, Arc<HashMap<u16, u16>>)
		>
	> // (sn, sender_id) => (msg => (multisig, signer_id => reps))
}

impl<S, P, M, H> MBRB for MultiSigBasedMBRB<S, P, M, H>
	where S: SecKey + 'static, P: PubKey + 'static, M: MsgAdversary + 'static, H: MultiSigHandler<P> + 'static
{
	fn mbrb_broadcast(&mut self, msg: &[u8], sn: u8) -> Result<(), String> {
		if msg.len() > MBRB_MSG_N_BYTES {
			return Err(format!("MBRB message too long: the maximum size is {MBRB_MSG_N_BYTES} bytes."));
		}
		if self.proc_has_signed_any(sn, self.proc_id) {
			return Err(format!("The process has already MBRB-broadcast a message for sequence number {sn}."))
		}

		let msg = Arc::new(msg.to_vec());
		let payload = Payload { msg: msg.clone(), sn, sender_id: self.proc_id };
		
		// save own signature
		let own_sig = self.sec_key.sign(&payload.to_bytes());
		let mut own_signer_ids_reps = HashMap::new();
		own_signer_ids_reps.insert(self.proc_id, 1);
		self.update_multisig(&payload, Arc::new(own_sig), Arc::new(own_signer_ids_reps));
		
		// ur-broadcast bundle
		let (multisig, signer_ids_reps) = self.get_saved_multisig(msg.clone(), sn, self.proc_id).unwrap();
		let bundle = BundleMultiSig { payload, multisig, signer_ids_reps };
		self.network.ur_broadcast(&bundle.to_bytes());

		match std::str::from_utf8(&msg) {
			Ok(msg_str) => {
				let msg_str = msg_str.trim_matches(char::from(0));
				info!("MBRB-broadcast '{msg_str}' with sequence number {sn}")
			},
			Err(_) => println!("MBRB-broadcast unreadable message with sequence number {sn}")
		};
		
		Ok(())
	}
}

impl<S, P, M, H> MultiSigBasedMBRB<S, P, M, H>
	where S: SecKey + 'static, P: PubKey + 'static, M: MsgAdversary + 'static, H: MultiSigHandler<P> + 'static
{
	pub fn new_mutex(network: Network<M>, t: u16, deliver_handler: Arc<dyn Fn(Vec<u8>, u8, u16) + Send + Sync>, proc_id: u16,
			sec_key: S, pub_keys: HashMap<u16, P>, multisig_handler: H) -> Arc<Mutex<MultiSigBasedMBRB<S, P, M, H>>> {
		let n = network.get_n();
		let d = network.get_d();
		if n <= 3*t + 2*d {
			panic!("MBRB necessary condition violated: n={} is inferior or equal to 3t+2d={}.", n, 3*t+2*d);
		}

		let delivered_msgs = HashSet::new();
		let saved_multisigs = HashMap::new();
		let mbrb = Arc::new(Mutex::new(MultiSigBasedMBRB {
			network, t, deliver_handler, proc_id, sec_key, pub_keys, multisig_handler, delivered_msgs, saved_multisigs
		}));

		let mbrb_cp = mbrb.clone();
		let receive_handler = Arc::new(move |msg: Vec<u8>| mbrb_cp.clone().lock().unwrap().handle_receive(&msg));
		let mbrb_cp = mbrb.clone();
		let mut guard = mbrb_cp.lock().unwrap();
		guard.network.set_receive_handler(receive_handler);
		guard.start_listening();
		mbrb
	}
	
	fn start_listening(&self) {
		self.network.start_listening();
	}

	fn handle_receive(&mut self, msg: &[u8]) {
		// if the message is a properly formatted bundle
		if let Some(bundle) = BundleMultiSig::from_bytes(msg) {
			let BundleMultiSig { payload, multisig, signer_ids_reps } = bundle;
			info!(
				"Received BUNDLE network message with payload sender {} and sequence number {} and {} signatures",
				payload.sender_id, payload.sn, signer_ids_reps.keys().len()
			);
			let app_msg = payload.msg.clone();
			let sn = payload.sn;
			let sender_id = payload.sender_id;

			// if an app-message was already mbrb-delivered for (sn, sender_id)
			if self.delivered_msgs.contains(&(sn, sender_id)) { return; }

			// if the bundle does not contain a signature for the sender
			if !signer_ids_reps.contains_key(&sender_id) { return; }
			
			// update the multisignature if the new one is valid and contains unsaved signatures
			let multisig_updated = self.update_multisig(&payload, multisig, signer_ids_reps);
			
			// if the multisignature was not updated
			if !multisig_updated { return; }

			let proc_has_not_signed = !self.proc_has_signed_any(payload.sn, payload.sender_id);
			
			// if the current process has not produced a signature for (sn, sender_id) yet
			if proc_has_not_signed {
				// save own signature
				let own_sig = self.sec_key.sign(&payload.to_bytes());
				let mut own_signer_ids_reps = HashMap::new();
				own_signer_ids_reps.insert(self.proc_id, 1);
				self.update_multisig(&payload, Arc::new(own_sig), Arc::new(own_signer_ids_reps));
			}

			let (multisig, signer_ids_reps) = self.get_saved_multisig(app_msg.clone(), sn, sender_id).unwrap();
			let n = self.network.get_n();
			let t = self.t;
			let quorum_is_reached = signer_ids_reps.len() > ((n+t)/2).into();

			// if either condition is satisfied
			if proc_has_not_signed || quorum_is_reached {
				// ur-broadcast bundle
				let bundle = BundleMultiSig {
					payload, multisig, signer_ids_reps
				};
				self.network.ur_broadcast(&bundle.to_bytes());
			}
			
			// if a quorum of signatures is reached for (msg, sn, sender_id)
			if quorum_is_reached {
				(self.deliver_handler)((*app_msg).clone(), sn, sender_id);
				self.delivered_msgs.insert((sn, sender_id));
			}
		} else {
			warn!("Received invalid network message");
		}
	}

	pub fn get_proc_id(&self) -> u16 { self.proc_id }

	fn proc_has_signed_any(&self, sn: u8, sender_id: u16) -> bool {
		match self.saved_multisigs.get(&(sn, sender_id)) {
			Some(msgs_map) => {
				for (_, (_, signer_ids_reps)) in msgs_map {
					if signer_ids_reps.contains_key(&self.proc_id) {
						return true;
					}
				}
				return false;
			},
			None => false
		}
	}

	fn get_saved_multisig(&self, msg: Arc<Vec<u8>>, sn: u8, sender_id: u16) -> Option<(Arc<Vec<u8>>, Arc<HashMap<u16, u16>>)> {
		if let Some(msgs_map) = self.saved_multisigs.get(&(sn, sender_id)) {
			if let Some((multisig, signer_ids_reps)) = msgs_map.get(&msg) {
				return Some((multisig.clone(), signer_ids_reps.clone()));
			} 
		}
		None
	}

	fn update_multisig(&mut self, payload: &Payload, other_multisig: Arc<Vec<u8>>, other_signer_ids_reps: Arc<HashMap<u16, u16>>) -> bool {
		let msg = payload.msg.clone();
		let sn = payload.sn;
		let sender_id = payload.sender_id;
		let key = (sn, sender_id);

		// check if the multisignature does not contain new signatures
		match self.saved_multisigs.get(&key) {
			Some(msgs_map) => {
				match msgs_map.get(&msg) {
					Some((_, saved_signer_ids_reps)) => {
						let saved_signer_ids: HashSet<u16> = saved_signer_ids_reps.keys().copied().collect();
						let received_signer_ids: HashSet<u16> = other_signer_ids_reps.keys().copied().collect();
						// if the multisignature contains no new signatures, return false
						if received_signer_ids.is_subset(&saved_signer_ids) { return false; }
					},
					None => ()
				}
			},
			None => ()
		}

		// construct the list of public keys with repetitions
		let mut pub_keys = vec![];
		// for each signer in the received message
		for (&signer_id, &reps) in other_signer_ids_reps.iter() {
			// if the signer is known, add its public key to the list
			if let Some(pub_key) = self.pub_keys.get(&signer_id) {
				let pub_key = pub_key.clone();
				for _ in 0..reps {
					pub_keys.push(pub_key.clone());
				}
			}
			// if the signer is not known, return false
			else { return false; }
		}

		// if the multisignature is not valid, return false
		if !self.multisig_handler.verify(&pub_keys, &payload.to_bytes(), &other_multisig) { return false; }

		// if some multisignatures for the given sender identity and sequence number were saved before
		if let Some(msgs_map) = self.saved_multisigs.get_mut(&key) {
			match msgs_map.remove(&msg) {
				// if a multisignature for the given app-message was saved before
				Some((saved_multisig, saved_signer_ids_reps)) => {
					let multisigs = vec![other_multisig.as_slice(), saved_multisig.as_slice()];
					// aggregate the received multisignature with the saved one
					if let Ok(new_multisig) = self.multisig_handler.aggregate(&multisigs) {
						let new_signer_ids_reps = merge_signer_ids_reps_maps(
							saved_signer_ids_reps.clone(), other_signer_ids_reps.clone()
						);
						msgs_map.insert(msg, (Arc::new(new_multisig), Arc::new(new_signer_ids_reps)));
					}
					// if the multisignatures cannot be correctly aggregated, return false
					else { return false; }
				},
				// if no multisignature was saved for the given app-message before, saved the received one
				None => { msgs_map.insert(msg, (other_multisig, other_signer_ids_reps)); }
			}
		}
		// if no multisignature was saved for the given sender identity and sequence number before
		else {
			// create a new message map associating the payload message with the multisignature
			let mut msgs_map = HashMap::new();
			msgs_map.insert(msg, (other_multisig, other_signer_ids_reps));
			// associate this new message map to the the given sender identity and sequence number
			self.saved_multisigs.insert(key, msgs_map);
		}

		// if the new multisignature was correctly saved, return true
		true
	}
}

fn merge_signer_ids_reps_maps(signer_ids_reps1: Arc<HashMap<u16, u16>>, signer_ids_reps2: Arc<HashMap<u16, u16>>) -> HashMap<u16, u16> {
	let mut new_signer_ids_reps = HashMap::new();

	let signer_ids_set1: HashSet<u16> = signer_ids_reps1.keys().copied().collect();
	let signer_ids_set2: HashSet<u16> = signer_ids_reps2.keys().copied().collect();
	let new_signer_ids_set = signer_ids_set1.union(&signer_ids_set2);

	for &signer_id in new_signer_ids_set {
		let reps1 = match signer_ids_reps1.get(&signer_id) {
			Some(&reps) => reps,
			None => 0
		};
		let reps2 = match signer_ids_reps2.get(&signer_id) {
			Some(&reps) => reps,
			None => 0
		};
		let new_reps = reps2 + reps1;
		new_signer_ids_reps.insert(signer_id, new_reps);
	}

	new_signer_ids_reps
}
