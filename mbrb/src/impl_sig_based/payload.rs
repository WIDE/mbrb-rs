use std::sync::Arc;

pub const MBRB_MSG_N_BYTES: usize = 32; // messages take up 256 bits

#[derive(Clone)]
pub struct Payload {
	pub msg: Arc<Vec<u8>>,
	pub sn: u8,
	pub sender_id: u16
}

impl Payload {
	pub fn to_bytes(&self) -> Vec<u8> {
		let mut bytes = self.msg.to_vec();
		bytes.resize(MBRB_MSG_N_BYTES, 0);
		let (v1, v2) = u16_to_two_u8(self.sender_id);
		bytes.push(self.sn);
		bytes.push(v1);
		bytes.push(v2);
		bytes
	}
}

/*
PAYLOAD MARSHALLING:
+-------------------------------------------------+
|          MSG (MBRB_MSG_N_BYTES bytes)           |
+------------------------+------------------------+
|      SN (1 byte)       |    SND_ID (2 bytes)    |
+------------------------+------------------------+
*/

pub fn two_u8_to_u16(v1: u8, v2: u8) -> u16 {
	((v1 as u16) << 8) | v2 as u16
}

pub fn u16_to_two_u8(v: u16) -> (u8, u8) {
	((v >> 8) as u8, v as u8)
}