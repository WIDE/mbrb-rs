use std::{
	net::SocketAddr,
	sync::Arc
};

pub trait Connection: Send {
	fn send(&self, dst: SocketAddr, msg: &[u8]);
	
	fn start_listening(&self);

	fn set_receive_handler(&mut self, handler: Arc<dyn Fn(Vec<u8>) + Send + Sync>);

	fn get_local_addr(&self) -> SocketAddr;
}
