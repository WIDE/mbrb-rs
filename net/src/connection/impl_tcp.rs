use std::{
	io::{Read, Write},
	net::{SocketAddr, TcpListener, TcpStream},
	thread,
	sync::Arc
};

use super::iface::Connection;

pub const MSG_MAX_SIZE: usize = 16384;

pub struct TCPConnection {
	listener: TcpListener,
	receive_handler: Arc<dyn Fn(Vec<u8>) + Send + Sync>
}

impl Connection for TCPConnection {
	fn send(&self, dst: SocketAddr, msg: &[u8]) {
		if msg.len() > MSG_MAX_SIZE {
			panic!("TCP message too long: the maximum size is {MSG_MAX_SIZE} bytes.");
		}
		//if dst == self.get_local_addr() {
		//	(self.receive_handler)(msg.to_vec());
		//} else {
			let mut stream = TcpStream::connect(dst).unwrap();
			stream.write(msg).unwrap();
		//}
	}

	fn start_listening(&self) {
		let listener = self.listener.try_clone().unwrap();
		let receive_handler = self.receive_handler.clone();
		thread::spawn(move || {
			for stream in listener.incoming() {
				let mut buf = [0; MSG_MAX_SIZE];
				stream.unwrap().read(&mut buf).unwrap();
				receive_handler(buf.to_vec());
			}
		});
	}

	fn set_receive_handler(&mut self, handler: Arc<dyn Fn(Vec<u8>) + Send + Sync>) {
		self.receive_handler = handler;
	}

	fn get_local_addr(&self) -> SocketAddr {
		self.listener.local_addr().unwrap()
	}
}

impl TCPConnection {
	pub fn new(src: SocketAddr) -> TCPConnection {
		TCPConnection {
			listener: TcpListener::bind(src).unwrap(),
			receive_handler: Arc::new(|_msg| ())
		}
	}
}