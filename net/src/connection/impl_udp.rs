use std::{
	net::{SocketAddr, UdpSocket},
	sync::Arc,
	thread
};

use super::iface::Connection;

pub const MSG_MAX_SIZE: usize = 1024;

pub struct UDPConnection {
	socket: UdpSocket,
	receive_handler: Arc<dyn Fn(Vec<u8>) + Send + Sync> // (msg) -> ()
}

impl Connection for UDPConnection {
	fn send(&self, dst: SocketAddr, msg: &[u8]) {
		if msg.len() > MSG_MAX_SIZE {
			panic!("UDP message too long: the maximum size is {MSG_MAX_SIZE} bytes to fit in a single UDP datagram.");
		}
		//if dst == self.get_local_addr() {
		//	(self.rcv_callback)(msg);
		//} else {
			self.socket.send_to(&msg, dst).unwrap();
		//}
	}

	fn start_listening(&self) {
		let socket = self.socket.try_clone().unwrap();
		let receive_handler = self.receive_handler.clone();
		thread::spawn(move || {
			loop {
				let mut buf = [0; MSG_MAX_SIZE];
				let (amount, _) = socket.recv_from(&mut buf).unwrap();
				receive_handler(buf[..amount].to_vec());
			}
		});
	}

	fn set_receive_handler(&mut self, handler: Arc<dyn Fn(Vec<u8>) + Send + Sync>) {
		self.receive_handler = handler;
	}

	fn get_local_addr(&self) -> SocketAddr {
		self.socket.local_addr().unwrap()
	}
}

impl UDPConnection {
	pub fn new(src: SocketAddr) -> UDPConnection {
		UDPConnection {
			socket: UdpSocket::bind(src).unwrap(),
			receive_handler: Arc::new(|_msg| ())
		}
	}
}
