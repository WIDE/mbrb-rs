use std::{
    collections::HashSet,
    net::SocketAddr
};

// Applicative Message Adversary implemented at the software layer
// An alternate solution could be to implement it at the network layer (using tools such as "tc filter")
pub trait MsgAdversary: Send {
	fn get_victims(&self, msg: &[u8]) -> HashSet<SocketAddr>;
}