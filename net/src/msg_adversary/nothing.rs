use std::net::SocketAddr;
use std::collections::HashSet;

use super::iface::MsgAdversary;

pub struct Nothing;

impl MsgAdversary for Nothing {
	fn get_victims(&self, _msg: &[u8]) -> HashSet<SocketAddr> {
		HashSet::new()
	}
}