use std::net::SocketAddr;
use std::collections::HashSet;

use rand::prelude::*;

use super::super::msg_adversary::MsgAdversaryStrat;

pub struct RandomTargetting {
	other_members: HashSet<SocketAddr>,
	d: usize
}

impl MsgAdversaryStrat for RandomTargetting {
	fn get_victims(&self, _: &[u8]) -> HashSet<SocketAddr> {
		let mut other_members_vec = Vec::from_iter(self.other_members.clone());
		let mut rng = rand::thread_rng();
		other_members_vec.shuffle(&mut rng);
		let victims = other_members_vec[0..self.d].to_vec();
		HashSet::from_iter(victims)
	}
}

impl RandomTargetting {
	pub fn new(src: SocketAddr, members: HashSet<SocketAddr>, d: usize) -> RandomTargetting {
		let mut other_members = members.clone();
		other_members.remove(&src);
		RandomTargetting { other_members, d }
	}
}
