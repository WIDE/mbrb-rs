use std::{
	collections::HashSet,
	net::SocketAddr,
	sync::Arc
};

use log::info;

use crate::{
	connection::iface::Connection,
	msg_adversary::iface::MsgAdversary
};

pub struct Network<M> where M: MsgAdversary
{
	connection: Box<dyn Connection>,
	members: HashSet<SocketAddr>,
	d: u16,
	msg_adversary: M
}

impl<M> Network<M> where M: MsgAdversary {
	pub fn new(connection: Box<dyn Connection>, members: HashSet<SocketAddr>, d: u16, msg_adversary: M) -> Network<M> {
		Network { connection, members, d, msg_adversary }
	}

	pub fn ur_broadcast(&self, msg: &[u8]) {
		let victims = self.msg_adversary.get_victims(msg);
		if victims.len() > self.d.into() {
			panic!("Too much message adversary victims: the provided strategy targets {} processes, but at most d={} are allowed.", victims.len(), self.d);
		}
		for dst in (&self.members).into_iter() {
			if !victims.contains(&dst) {
				self.connection.send(dst.to_owned(), msg);
			}
		}
		info!("UR-broadcast network message");
	}

	pub fn set_receive_handler(&mut self, receive_handler: Arc<dyn Fn(Vec<u8>) + Send + Sync>) {
		self.connection.set_receive_handler(receive_handler);
	}

	pub fn get_n(&self) -> u16 {
		self.members.len() as u16
	}

	pub fn get_d(&self) -> u16 {
		self.d
	}

	pub fn start_listening(&self) {
		self.connection.start_listening();
	}
}
