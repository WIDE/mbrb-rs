use std::{
	collections::{VecDeque, HashMap},
	sync::Arc
};

use super::{
	super::iface::{SN_N_BYTES, PROC_ID_N_BYTES},
	payload::*
};

pub struct BundleSig {
	pub payload: Payload,
	pub sigs: HashMap<u16, Arc<Vec<u8>>> // signer_id => sig
}

/*
BUNDLE MARSHALLING:
+--------------------------+
|            MSG           |
+--------+--------+--------+
|   SN   | SND_ID | N_SIGS |
+--------+--------+--------+
|                          |
|       SIG COUPLES        |
|                          |
+--------------------------+

SIGNATURE COUPLE MARSHALLING:
+---------+---------+
| PROC_ID | SIG_LEN |
+---------+---------+
|                   |
|        SIG        |
|                   |
+-------------------+
*/

impl BundleSig {
	pub fn to_bytes(&self) -> Vec<u8> {
		let mut bytes = self.payload.to_bytes();
		
		// there are at most as many signatures as there are processes in the system
		let n_sigs = self.sigs.len() as u16;
		let (v1, v2) = u16_to_two_u8(n_sigs);
		bytes.push(v1);
		bytes.push(v2);
		
		for (signer_id, sig) in self.sigs.clone() {
			let (v1, v2) = u16_to_two_u8(signer_id);
			let sig_len = sig.len() as u8;
			bytes.push(v1);
			bytes.push(v2);
			bytes.push(sig_len);
			bytes.append(&mut sig.to_vec());
		}

		bytes
	}

	pub fn from_bytes(bytes: &[u8]) -> Option<BundleSig> {
		let mut bytes = VecDeque::from(bytes.to_vec());

		// if there are not enough bytes for the fixed-size fields
		if bytes.len() < MBRB_MSG_N_BYTES+SN_N_BYTES+2*PROC_ID_N_BYTES {
			return None;
		}

		// extract payload
		let msg = Arc::new(bytes.drain(0..MBRB_MSG_N_BYTES).collect());
		let sn = bytes.pop_front().unwrap();
		let sender_id = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
		let payload = Payload { msg, sn, sender_id };
		
		// extract signatures
		let n_sigs = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
		let mut sigs = HashMap::new();
		for _ in 0..n_sigs {
			// if there are not enough bytes for the fixed-size fields of each signature
			if bytes.len() < PROC_ID_N_BYTES+SIG_LEN_N_BYTES {
				return None;
			}

			let signer_id = two_u8_to_u16(bytes.pop_front().unwrap(), bytes.pop_front().unwrap());
			let sig_len = bytes.pop_front().unwrap();

			// if there are not enough bytes for the signature
			if bytes.len() < sig_len.into() {
				return None;
			}

			let mut sig = vec![];
			for _ in 0..sig_len {
				sig.push(bytes.pop_front().unwrap());
			}
			sigs.insert(signer_id, Arc::new(sig));
		}

		Some(BundleSig { payload, sigs })
	}
}
