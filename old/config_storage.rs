use std::{path::PathBuf, fs};

use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ConfigRecord {
	pub name: String,
	pub ip_version: u8,
	pub transport: TransportProtocol,
	pub t: u16,
	pub d: u16,
	pub pub_keys_folder: PathBuf,
	pub sec_keys_folder: PathBuf,
	pub log_folder: PathBuf,
	pub processes: Vec<ProcessInfo>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProcessInfo {
	pub proc_id: u16,
	pub ip_addr: String,
	pub port: u16
}

#[derive(Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum TransportProtocol { UDP, TCP }

pub fn read_config(config_file_path: PathBuf) -> ConfigRecord {
	let json = fs::read_to_string(config_file_path).unwrap();
	serde_json::from_str(&json).unwrap()
}