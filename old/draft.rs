use std::{collections::{HashSet, HashMap}, net::{SocketAddrV6, SocketAddrV4, SocketAddr}};

use crate::{
	storage::{keys_storage::KeyStorage, config_storage::ConfigRecord},
	crypto::{impl_bls::{BLSKeyGen, BLSMultiSigHandler}, iface::{SecKey, MultiSigHandler}}
};

pub fn draft(config: ConfigRecord) {
	// CREATE IMPORTANT VARIABLES
	let multisig_handler = BLSMultiSigHandler;
	let key_storage = KeyStorage::new(config.sec_keys_folder, config.pub_keys_folder, BLSKeyGen);

	// CREATE PROCESSES MAP
	let processes_map: HashMap<u16, SocketAddr> = config.processes.into_iter().map(
        |process| (process.proc_id, match config.ip_version {
            4 => SocketAddr::V4(SocketAddrV4::new(process.ip_addr.parse().unwrap(), process.port)),
            6 => SocketAddr::V6(SocketAddrV6::new(process.ip_addr.parse().unwrap(), process.port, 0, 0)),
            _ => panic!("Invalid IP version")
        })
    ).collect();
	let proc_ids_set: HashSet<u16> = processes_map.keys().cloned().collect();

	// POTENTIALLY RECREATE KEY PAIRS
	let recreate_keys = true;
	if recreate_keys {
		let n = proc_ids_set.len() as u16;
		key_storage.create_key_pairs(n);
	}

	// READ KEY PAIRS FROM FILES
	let pub_keys = key_storage.read_pub_keys(&proc_ids_set);

	let pub_key1 = pub_keys.get(&1).unwrap().clone();
	let pub_key2 = pub_keys.get(&2).unwrap().clone();
	let pub_key3 = pub_keys.get(&3).unwrap().clone();
	let pub_key4 = pub_keys.get(&4).unwrap().clone();

	let sec_key1 = key_storage.read_sec_key(1);
	let sec_key2 = key_storage.read_sec_key(2);
	let sec_key3 = key_storage.read_sec_key(3);
	let sec_key4 = key_storage.read_sec_key(4);

	// SIGN MESSAGES
	/* *
	let msg1 = "message1".as_bytes();
	let msg2 = "message2".as_bytes();
	let msg3 = "message3".as_bytes();
	let msg4 = "message4".as_bytes();
	/ */
	let msg = "message bonjour".as_bytes();

	let sig1 = sec_key1.sign(msg);
	let sig2 = sec_key2.sign(msg);
	let sig3 = sec_key3.sign(msg);
	let sig4 = sec_key4.sign(msg);

	// AGGREGATE SIGNATURES
	let sigs12 = vec![sig1.as_slice(), sig2.as_slice()];
	let sigs23 = vec![sig2.as_slice(), sig3.as_slice()];
	let sigs34 = vec![sig3.as_slice(), sig4.as_slice()];

	let multisig12 = multisig_handler.aggregate(&sigs12).unwrap();
	let multisig23 = multisig_handler.aggregate(&sigs23).unwrap();
	let multisig34 = multisig_handler.aggregate(&sigs34).unwrap();
	let multisig1223 = multisig_handler.aggregate(&vec![multisig12.as_slice(), multisig23.as_slice()]).unwrap();
	let multisig1234 = multisig_handler.aggregate(&vec![multisig12.as_slice(), multisig34.as_slice()]).unwrap();
	
	// VERIFY MULTISIGNATURES
	/* *
	let msgs12 = vec![msg1, msg2];
	let msgs23 = vec![msg2, msg3];
	let msgs123 = vec![msg1, msg2, msg3];
	let msgs1223 = vec![msg1, msg2, msg2, msg3];
	let msgs1234 = vec![msg1, msg2, msg3, msg4];
	let msgs4132 = vec![msg4, msg1, msg3, msg2];
	/ */

	let pub_keys12 = vec![pub_key1.clone(), pub_key2.clone()];
	let pub_keys23 = vec![pub_key2.clone(), pub_key3.clone()];
	let pub_keys123 = vec![pub_key1.clone(), pub_key2.clone(), pub_key3.clone()];
	let pub_keys1223 = vec![pub_key1.clone(), pub_key2.clone(), pub_key2.clone(), pub_key3.clone()];
	let pub_keys1234 = vec![pub_key1.clone(), pub_key2.clone(), pub_key3.clone(), pub_key4.clone()];
	let pub_keys4132 = vec![pub_key4.clone(), pub_key1.clone(), pub_key3.clone(), pub_key2.clone()];

	let valid12 = multisig_handler.verify(&pub_keys12, &msg, &multisig12);
	let valid23 = multisig_handler.verify(&pub_keys23, &msg, &multisig23);
	let valid123 = multisig_handler.verify(&pub_keys123, &msg, &multisig1223);
	let valid1223 = multisig_handler.verify(&pub_keys1223, &msg, &multisig1223);
	let valid1234 = multisig_handler.verify(&pub_keys1234, &msg, &multisig1234);
	let valid4132 = multisig_handler.verify(&pub_keys4132, &msg, &multisig1234);

	println!("keys:\tsigs:\t");
	println!("12\t12\t{}", valid12);
	println!("23\t23\t{}", valid23);
	println!("123\t1223\t{}", valid123);
	println!("1223\t1223\t{}", valid1223);
	println!("1234\t1234\t{}", valid1234);
	println!("4132\t1234\t{}", valid4132);
}