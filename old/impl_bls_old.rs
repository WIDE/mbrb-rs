use bls_signatures::{PrivateKey, PublicKey, Serialize, Signature};
use rand_core::OsRng;

use super::iface::{KeyGen, SecKey, PubKey, Key, MultiSigHandler};

pub struct BLSKeyGen;

impl KeyGen<BLSSecKey, BLSPubKey> for BLSKeyGen {
	fn generate(&self) -> (BLSSecKey, BLSPubKey) {
		let sig_key = PrivateKey::generate(&mut OsRng);
		let priv_key = BLSSecKey { sec_key: sig_key };
		let pub_key = BLSPubKey { pub_key: sig_key.public_key() };
		(priv_key, pub_key)
	}

	fn sec_key_from_bytes(&self, b: &[u8]) -> BLSSecKey {
		BLSSecKey { sec_key: PrivateKey::from_bytes(b).unwrap() }
	}

	fn pub_key_from_bytes(&self, b: &[u8]) -> BLSPubKey {
		BLSPubKey { pub_key: PublicKey::from_bytes(b).unwrap() }
	}
}

pub struct BLSSecKey {
	sec_key: PrivateKey
}

impl SecKey for BLSSecKey {
	fn sign(&self, msg: &[u8]) -> Vec<u8> {
		self.sec_key.sign(msg).as_bytes()
	}
}

impl Key for BLSSecKey {
	fn to_bytes(&self) -> Vec<u8> {
		self.sec_key.as_bytes()
	}
}

#[derive(Clone)]
pub struct BLSPubKey {
	pub_key: PublicKey
}

impl PubKey for BLSPubKey {
	fn verify(&self, msg: &[u8], sig: &[u8]) -> bool {
		match Signature::from_bytes(sig) {
			Ok(sig) => self.pub_key.verify(sig, msg),
			Err(_) => false
		}
	}
}

impl Key for BLSPubKey {
	fn to_bytes(&self) -> Vec<u8> {
		self.pub_key.as_bytes()
	}
}

pub struct BLSMultiSigHandler;

impl MultiSigHandler<BLSPubKey> for BLSMultiSigHandler {
	fn aggregate(&self, sigs: &[&[u8]]) -> Result<Vec<u8>, String> {
		let mut sigs_vec = vec![];
		for sig in sigs {
			match Signature::from_bytes(sig) {
				Ok(sig) => multisigs_vec.push(sig),
				Err(_) => return Err(String::from("Some signatures cannot be decoded from bytes."))
			}
		}
		match bls_signatures::aggregate(&sigs_vec) {
			Ok(multisig) => Ok(multisig.as_bytes()),
			Err(_) => Err(String::from("Signatures cannot be aggregated."))
		}
	}

	fn verify(&self, pub_keys: &[BLSPubKey], msgs: &[&[u8]], multisig: &[u8]) -> bool {
		let mut pub_keys_vec = vec![];
		for pub_key in pub_keys {
			pub_keys_vec.push(pub_key.pub_key);
		}
		match Signature::from_bytes(multisig) {
			Ok(multisig) => bls_signatures::verify_messages(&multisig, msgs, &pub_keys_vec),
			Err(_) => false
		}
    }
}
