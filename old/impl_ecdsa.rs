use rand_core::OsRng;
use p256::{
	ecdsa::{
		Signature,
		SigningKey, signature::Signer,
		VerifyingKey, signature::Verifier
	},
	EncodedPoint
};

use super::iface::*;

pub struct ECDSAKeyGen;

impl KeyGen<ECDSASecKey, ECDSAPubKey> for ECDSAKeyGen {
	fn generate(&self) -> (ECDSASecKey, ECDSAPubKey) {
		let sig_key = SigningKey::random(&mut OsRng);
		let pub_key = ECDSAPubKey { pub_key: VerifyingKey::from(&sig_key) };
		let priv_key = ECDSASecKey { sec_key: sig_key };
		(priv_key, pub_key)
	}

	fn sec_key_from_bytes(&self, b: &[u8]) -> ECDSASecKey {
		let sig_key = SigningKey::from_bytes(b).unwrap();
		ECDSASecKey { sec_key: sig_key }
	}

	fn pub_key_from_bytes(&self, b: &[u8]) -> ECDSAPubKey {
		let point = &EncodedPoint::from_bytes(b).unwrap();
		let vrf_key = VerifyingKey::from_encoded_point(point).unwrap();
		ECDSAPubKey { pub_key: vrf_key }
	}
}

pub struct ECDSASecKey {
	sec_key: SigningKey
}

impl SecKey for ECDSASecKey {
	fn sign(&self, msg: &[u8]) -> Vec<u8> {
		// Caveat: DER-encoded ECDSA signatures can have a size varying from 71 to 73 bytes
		// see: https://transactionfee.info/charts/bitcoin-script-ecdsa-length/
		self.sec_key.sign(msg).to_der().as_bytes().to_vec()
	}
}

impl Key for ECDSASecKey {
	fn to_bytes(&self) -> Vec<u8> {
		self.sec_key.to_bytes().to_vec()
	}
}

#[derive(Clone)]
pub struct ECDSAPubKey {
	pub_key: VerifyingKey
}

impl PubKey for ECDSAPubKey {
	fn verify(&self, msg: &[u8], sig: &[u8]) -> bool {
		match Signature::from_der(sig) {
			Ok(sig) => self.pub_key.verify(msg, &sig).is_ok(),
			Err(_) => false
		}
	}
}

impl Key for ECDSAPubKey {
	fn to_bytes(&self) -> Vec<u8> {
		VerifyingKey::to_encoded_point(&self.pub_key, true).as_bytes().to_vec()
	}
}
