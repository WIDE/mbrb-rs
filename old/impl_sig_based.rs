use std::{
	collections::{HashMap, HashSet},
	sync::{Arc, Mutex}
};

use crate::{
	net::{msg_adversary::{MsgAdversary, MsgAdversaryStrat}, iface::Connection},
	crypto::iface::*
};
use super::{
	super::iface::*,
	payload::*,
	bundle_sig::*
};

pub const SIG_LEN_N_BYTES: usize = 1;

pub struct SigBasedMBRB<S, P, C, M>
	where S: SecKey, P: PubKey, C: Connection, M: MsgAdversaryStrat
{
	msg_adversary: MsgAdversary<C, M>,
	t: u16,
	deliver_handler: Arc<dyn Fn(Vec<u8>, u8, u16) + Send + Sync>, // (msg, sn, sender_id) -> ()
	proc_id: u16,
	sec_key: S,
	pub_keys: HashMap<u16, P>,
	delivered_msgs: HashSet<(u8, u16)>, // { (sn, sender_id) }
	saved_sigs: HashMap<(u8, u16), HashMap<Arc<Vec<u8>>, HashMap<u16, Arc<Vec<u8>>>>> // (sn, sender_id) => (msg => (signer_id => sig))
}

impl<S, P, C, M> MBRB for SigBasedMBRB<S, P, C, M>
	where S: SecKey + 'static, P: PubKey + 'static, C: Connection + 'static, M: MsgAdversaryStrat + 'static
{
	fn mbrb_broadcast(&mut self, msg: &[u8], sn: u8) -> Result<(), String> {
		if msg.len() > MBRB_MSG_N_BYTES {
			return Err(format!("MBRB message too long: the maximum size is {MBRB_MSG_N_BYTES} bytes."));
		}
		if self.proc_has_signed_any(sn, self.proc_id) {
			return Err(format!("The process has already MBRB-broadcast a message for sequence number {sn}."))
		}

		let msg = Arc::new(msg.to_vec());
		let payload = Payload { msg: msg.clone(), sn, sender_id: self.proc_id };
		
		// save own signature
		let sig = Arc::new(self.sec_key.sign(&payload.to_bytes()));
		self.save_sig(msg.clone(), sn, self.proc_id, self.proc_id, sig);
		
		// ur-broadcast bundle
		let bundle = BundleSig {
			payload,
			sigs: self.get_saved_sigs(&msg, sn, self.proc_id)
		};
		self.msg_adversary.ur_broadcast(&bundle.to_bytes());
		
		Ok(())
	}
}

impl<S, P, C, M> SigBasedMBRB<S, P, C, M>
	where S: SecKey + 'static, P: PubKey + 'static, C: Connection + 'static, M: MsgAdversaryStrat + 'static
{
	pub fn new_mutex(msg_adversary: MsgAdversary<C, M>, t: u16, deliver_handler: Arc<dyn Fn(Vec<u8>, u8, u16) + Send + Sync>, proc_id: u16,
			sec_key: S, pub_keys: HashMap<u16, P>) -> Arc<Mutex<SigBasedMBRB<S, P, C, M>>> {
		let n = msg_adversary.get_n();
		let d = msg_adversary.get_d();
		if n <= 3*t + 2*d {
			panic!("MBRB necessary condition violated: n={} is inferior or equal to 3t+2d={}.", n, 3*t+2*d);
		}

		let delivered_msgs = HashSet::new();
		let saved_sigs = HashMap::new();
		let mbrb = Arc::new(Mutex::new(SigBasedMBRB {
			msg_adversary, t, deliver_handler, proc_id, sec_key, pub_keys, delivered_msgs, saved_sigs
		}));

		let mbrb_cp = mbrb.clone();
		let receive_handler = Arc::new(move |msg: Vec<u8>| mbrb_cp.clone().lock().unwrap().handle_receive(&msg));
		let mbrb_cp = mbrb.clone();
		let mut guard = mbrb_cp.lock().unwrap();
		guard.msg_adversary.set_receive_handler(receive_handler);
		guard.start_listening();
		mbrb
	}
	
	fn start_listening(&self) {
		self.msg_adversary.start_listening();
	}

	fn handle_receive(&mut self, msg: &[u8]) {
		// if the message is a bundle
		if let Some(bundle) = BundleSig::from_bytes(msg) {
			let BundleSig { payload, sigs } = bundle;

			// if an app-message was already mbrb-delivered for (sn, sender_id)
			if self.delivered_msgs.contains(&(payload.sn, payload.sender_id)) { return; }

			// if the sender is unknown
			if !self.pub_keys.contains_key(&payload.sender_id) { return; }
			let sender_pub_key = self.pub_keys.get(&payload.sender_id).unwrap();
			
			// if the bundle does not contain the sender signature
			if !sigs.contains_key(&payload.sender_id) { return; }
			let sender_sig = sigs.get(&payload.sender_id).unwrap().clone();
			
			// if the sender signature is invalid
			if !sender_pub_key.verify(&payload.to_bytes(), &sender_sig) { return; }

			// save unsaved signatures
			let new_sigs_saved = self.save_valid_unsaved_sigs(payload.clone(), sigs);

			// if no new signatures were saved
			if !new_sigs_saved { return; }

			let proc_has_not_signed = !self.proc_has_signed_any(payload.sn, payload.sender_id);
			
			// if the current process has not produced a signature for (sn, sender_id) yet
			if proc_has_not_signed {
				// save own signature
				let sig = Arc::new(self.sec_key.sign(&payload.to_bytes()));
				self.save_sig(payload.msg.clone(), payload.sn, payload.sender_id, self.proc_id, sig);
			}

			let saved_sigs = self.get_saved_sigs(&payload.msg, payload.sn, payload.sender_id);
			let n = self.msg_adversary.get_n();
			let t = self.t;
			let quorum_is_reached = saved_sigs.len() > ((n+t)/2).into();

			// if either condition is satisfied
			if proc_has_not_signed || quorum_is_reached {
				// ur-broadcast bundle
				let bundle = BundleSig {
					payload: payload.clone(),
					sigs: self.get_saved_sigs(&payload.msg, payload.sn, payload.sender_id)
				};
				self.msg_adversary.ur_broadcast(&bundle.to_bytes());
			}
			
			// if a quorum of signatures is reached for (msg, sn, sender_id)
			if quorum_is_reached {
				let msg = (*payload.msg).clone();
				(self.deliver_handler)(msg, payload.sn, payload.sender_id);
				self.delivered_msgs.insert((payload.sn, payload.sender_id));
			}
		}
	}

	pub fn get_proc_id(&self) -> u16 { self.proc_id }

	fn proc_has_signed_any(&self, sn: u8, sender_id: u16) -> bool {
		let key = &(sn, sender_id);
		match self.saved_sigs.get(key) {
			Some(msgs_map) => {
				for (_, signers_map) in msgs_map {
					if signers_map.contains_key(&self.proc_id) {
						return true;
					}
				}
				return false;
			},
			None => false
		}
	}

	fn sig_is_saved(&self, msg: Arc<Vec<u8>>, sn: u8, sender_id: u16, signer_id: u16) -> bool {
		match self.saved_sigs.get(&(sn, sender_id)) {
			Some(msgs_map) => match msgs_map.get(&msg) {
				Some(signers_map) => signers_map.contains_key(&signer_id),
				None => false
			},
			None => false
		}
	}

	fn save_sig(&mut self, msg: Arc<Vec<u8>>, sn: u8, sender_id: u16, signer_id: u16, sig: Arc<Vec<u8>>) {
		let key = (sn, sender_id);
		if !self.saved_sigs.contains_key(&key) {
			self.saved_sigs.insert(key, HashMap::new());
		}
		let msgs_map = self.saved_sigs.get_mut(&key).unwrap();
		if !msgs_map.contains_key(&msg) {
			msgs_map.insert(msg.clone(), HashMap::new());
		}
		let signers_map = msgs_map.get_mut(&msg).unwrap();
		signers_map.insert(signer_id, sig);
	}

	fn get_saved_sigs(&self, msg: &Arc<Vec<u8>>, sn: u8, sender_id: u16) -> HashMap<u16, Arc<Vec<u8>>> {
		let key = &(sn, sender_id);
		if !self.saved_sigs.contains_key(key) {
			return HashMap::new();
		}
		let msgs_map_ref = self.saved_sigs.get(key).unwrap().clone();
		if !msgs_map_ref.contains_key(msg) {
			return HashMap::new();
		}
		msgs_map_ref.get(msg).unwrap().clone()
	}

	fn save_valid_unsaved_sigs(&mut self, payload: Payload, sigs: HashMap<u16, Arc<Vec<u8>>>) -> bool {
		let mut new_sigs_saved = false;
		for (signer_id, sig) in sigs {
			if let Some(pub_key) = self.pub_keys.get(&signer_id.into()) {
				if !self.sig_is_saved(payload.msg.clone(), payload.sn, payload.sender_id.into(), signer_id)
						&& pub_key.verify(&payload.to_bytes(), &sig) {
					self.save_sig(payload.msg.clone(), payload.sn, payload.sender_id, signer_id, sig);
					new_sigs_saved = true;
				}
			}
		}
		new_sigs_saved
	}
}
