use std::{
    collections::HashMap,
    net::{SocketAddr, SocketAddrV4},
	path::Path
};

use csv;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct MemberRecord {
	proc_id: u16,
	ip_addr: String,
	port: u16
}

pub fn read_members<P>(members_file_path: P) -> HashMap<u16, SocketAddr>
	where P: AsRef<Path>
{
    let mut reader = csv::Reader::from_path(members_file_path).unwrap();
	let mut members_map = HashMap::new();
	for result in reader.deserialize() {
		let member: MemberRecord = result.unwrap();
		let addr = SocketAddr::V4(SocketAddrV4::new(member.ip_addr.parse().unwrap(), member.port));
		members_map.insert(member.proc_id, addr);
	}
    members_map
}