use std::{
	collections::HashSet,
	net::SocketAddr,
	sync::Arc,
	thread,
	time,
	str,
	env,
	path::Path
};

use crate::{
	mbrb::{impl_sig_based::impl_multisig_based::MultiSigBasedMBRB, iface::MBRB},
	crypto::impl_bls::{BLSKeyGen, BLSMultiSigHandler},
	net::{
		impl_tcp::TCPConnection,
		msg_adversary::MsgAdversary,
		msg_adversary_strats::nothing::Nothing
	},
	storage::{
		keys_storage::KeyStorage,
		members_storage::read_members
	}
};

fn get_default_t_d(n: u16) -> (u16, u16) {
	((n-1)/6, (n-1)/4)
}

pub fn test() {
	let recreate_keys = true;

	let current_dir_path = env::current_dir().unwrap();
	let members_file_path = current_dir_path.join(Path::new("res/members.csv"));
	let sec_keys_dir_path = current_dir_path.join(Path::new("res/key_pairs/sec"));
	let pub_keys_dir_path = current_dir_path.join(Path::new("res/key_pairs/pub"));

	let members_map = read_members(members_file_path);
	let members_id_set: HashSet<u16> = members_map.keys().cloned().collect();
	let members_addr_set: HashSet<SocketAddr> = members_map.values().cloned().collect();

	let n = members_addr_set.len() as u16;
	let (t, d) = get_default_t_d(n);

	let key_storage = KeyStorage::new(sec_keys_dir_path, pub_keys_dir_path, BLSKeyGen);
	if recreate_keys {
		key_storage.create_key_pairs(n);
	}
	let pub_keys = key_storage.read_pub_keys(&members_id_set);

	let mut mbrb_instances = vec![];
	for (proc_id, addr) in members_map {
		let conn = TCPConnection::new(addr);
		let strat = Nothing;
		let msg_adversary = MsgAdversary::new(conn, members_addr_set.clone(), d, strat);
		let priv_key = key_storage.read_sec_key(proc_id);
		let multisig_handler = BLSMultiSigHandler;
		
		let deliver_handler = move |msg: Vec<u8>, sn: u8, sender_id: u16| {
			match str::from_utf8(&msg) {
				Ok(msg_str) => println!("Process {proc_id}: mbrb-delivered '{msg_str}' with sequence number {sn} from process {sender_id}."),
				Err(_) => println!("Process {proc_id}: mbrb-delivered an unreadable message with sequence number {sn} from process {sender_id}.")
			};
		};
		
		let mbrb = MultiSigBasedMBRB::new_mutex(
			msg_adversary, t, Arc::new(deliver_handler), proc_id, priv_key, pub_keys.clone(), multisig_handler
		);
		println!("MBRB instance of process {proc_id} launched.");

		mbrb_instances.push(mbrb);
	}
	println!();

	let mbrb = mbrb_instances[0].clone();
	let msg = "hello world";
	let sn = 1;
	let sender_id = mbrb.clone().lock().unwrap().get_proc_id();
	println!("Process {sender_id}: mbrb-broadcast '{msg}' with sequence number {sn}.");
	println!();
	mbrb.lock().unwrap().mbrb_broadcast(msg.as_bytes(), sn).unwrap();

	let dur = time::Duration::from_secs(1);
	thread::sleep(dur);
}
