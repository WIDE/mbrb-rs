

# global_config_json = json_minify(global_config)
# run_config_json = json_minify(run_config)

# n_threads = multiprocessing.cpu_count()
# pools = split(processes, n_threads)

# def run_processes(en, pool):
#     for proc_info in processes:
#         proc_id = proc_info['procId']
#         run_cmd = f"cargo run -- expe '{global_config_json}' '{run_config_json}' {proc_id} {log_folder_path}"
#         cmd = f"cd {GIT_REPO_NAME}; {run_cmd}"
#         print(cmd)
#         en.run_command(cmd, pattern_hosts=proc_info["addr"], roles=roles, background=True)


# threads = []
# for pool in pools:
#     thread = Thread(target=run_processes, args=(en, pool))
#     threads.append(thread)
#     thread.start()

# for thread in threads:
#     thread.join()

# sleep_time_before_sec = int(global_config["actions"]["before"][0][1])
# sleep_time_after_sec = int(global_config["actions"]["after"][0][1])
# sleep_time_sec = sleep_time_before_sec + sleep_time_after_sec
# time.sleep(sleep_time_sec)


def select_geo_distributed(selectable_nodes, n):
    selected_nodes = {}
    n_selected_global = 0
    # select n geo-distributed nodes among clusters
    while True:
        for site_name, clusters in selectable_nodes.items():
            if site_name not in selected_nodes:
                    selected_nodes[site_name] = {}
            for cluster_name, n_nodes_max in clusters.items():
                if cluster_name not in selected_nodes[site_name]:
                    selected_nodes[site_name][cluster_name] = 0
                # if all available nodes are selected, pass to the next cluster
                if selected_nodes[site_name][cluster_name] >= n_nodes_max:
                    continue
                selected_nodes[site_name][cluster_name] += 1
                n_selected_global += 1
                # if all n nodes are selected, exit
                if n_selected_global >= n:
                    break
            if n_selected_global >= n:
                break
        if n_selected_global >= n:
            break
    return selected_nodes

def select_centralized(n_selectable_nodes, n, cities):
    selected_nodes = {}
    n_selected_global = 0
    # select n nodes among clusters in the most centralized manner
    while True:
        for site_name, clusters in n_selectable_nodes.items():
            selected_nodes[site_name] = {}
            for cluster_name, n_nodes_max in clusters.items():
                if n-n_selected_global >= n_nodes_max:
                    n_selected_cluster = n_nodes_max
                else:
                    n_selected_cluster = n-n_selected_global
                selected_nodes[site_name][cluster_name] = n_selected_cluster
                n_selected_global += n_selected_cluster
                if n_selected_global >= n:
                    break
            if n_selected_global >= n:
                break
        if n_selected_global >= n:
            break
    return selected_nodes


# N_SELECTABLE_NODES = {
#     "rennes": { "paravance": 20, "parasilo": 8 },
#     "nantes": { "econome": 10, "ecotype": 20 },
#     "luxembourg": { "petitprince": 5 },
#     "nancy": { "gros": 20, "grisou": 10 },
#     "lyon": { "nova": 5 },
#     "lille": { "chetemi": 5, "chiclet": 2, "chifflet": 2, "chifflot": 2 },
#     "grenoble": { "dahu": 10 },
#     "sophia": { "uvb": 10 }
# }

# sum([n_nodes for site_clusters in N_SELECTABLE_NODES.values() for n_nodes in site_clusters.values()])

from threading import Thread
import multiprocessing

def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i*k+min(i, m):(i+1)*k+min(i+1, m)] for i in range(n))

USED_CITIES = [
    "Paris", "London", "Frankfurt", "Moscow", "Warsaw", "Toronto",
    "NewYork", "SanFrancisco", "Mexico", "BuenosAires", "Brasilia",
    "Johannesburg", "Singapore", "Tokyo", "Seoul", "Shanghai",
    "HongKong", "NewDelhi", "Melbourne", "Auckland"
]