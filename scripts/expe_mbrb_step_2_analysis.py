#/bin/python.exe

import os
import json
from datetime import datetime
import sys
import re


def get_n(timestamp, local_expe_folder_path):
	dt = datetime.fromisoformat(timestamp)
	if dt.year < 2010:
		run_type = "local"
	else:
		run_type = "g5k"
	parent_config_folder_path = f"{local_expe_folder_path}/config"
	run_config_path = f"{parent_config_folder_path}/run/{run_type}/run_config-{timestamp}.json"
	with open(run_config_path, "r") as f:
		global_config_name = json.load(f)["globalConfig"]
	global_config_path = f"{parent_config_folder_path}/global/global_config-{global_config_name}.json"
	with open(global_config_path, "r") as f:
		n = int(json.load(f)["n"])
	return n


def get_logs_paths(log_folder_path):
	log_file_paths = []

	for filename in os.listdir(log_folder_path):
		if filename.endswith(".log"):
			log_file_paths.append(os.path.join(log_folder_path, filename))
	
	return log_file_paths


def get_exec_events_from_logs(log_file_paths):
	# The exec_events dictionary associates for each message id (process id+sequence number couple identifying an execution)
	# that appears in the logs the list of events (ordered by timestamp) that are related to this execution of MBRB.
	# An event is of the form (timestamp, process id, log message).
	exec_events = {}

	for log_file_path in log_file_paths:
		with open(log_file_path, "r") as f:
			lines = f.read().splitlines()
		match = re.search(r"p_[0-9]+\.log", log_file_path).group()
		proc_id = int(match[2:-4])
		for line in lines:
			timestamp_str, _, log_msg = line.split(" | ")
			# if the log message is not a MBRB-broadcast/delivery, skip
			if not log_msg.startswith("MBRB-"):
				continue
			msg_id = get_msg_id_from_log_msg(log_msg, proc_id)
			if msg_id not in exec_events:
				exec_events[msg_id] = []
			timestamp = datetime.fromisoformat(timestamp_str)
			# construct the event
			event = (timestamp, proc_id, log_msg)
			# find the index for inserting the event
			idx = 0
			for inserted_timestamp, _, _ in exec_events[msg_id]:
				if inserted_timestamp >= timestamp:
					break
				idx += 1
			# insert the event in the correct position
			exec_events[msg_id].insert(idx, event)
	
	return exec_events


def get_msg_id_from_log_msg(log_msg, proc_id):
	# Get the message id (that is, the couple (sender id, sequence number) of a MBRB message
	# which identifies an execution of MBRB) from a log message.
	if log_msg.startswith("MBRB-broadcast"):
		snd_id = proc_id
		sn_str = re.sub(r"MBRB-broadcast '.*' with sequence number ", "", log_msg)
		sn = int(sn_str)
	elif log_msg.startswith("MBRB-delivered"):
		snd_id_str = re.sub(r"MBRB-delivered '.*' with sequence number .* from process ", "", log_msg)
		snd_id = int(snd_id_str)
		sn_str = re.sub(r"MBRB-delivered '.*' with sequence number ", "", log_msg)
		sn_str = re.sub(r" from process .*", "", sn_str)
		sn = int(sn_str)
	return snd_id, sn


def compute_exec_latencies(exec_events):
	# The exec_latencies dictionary associates for each message id (i.e. (process id,sequence number) couple identifying an execution)
	# the list of latencies (i.e. the time between the mbrb-broadcast and the mbrb-deliver, in seconds) of each process for each
	# execution of MBRB, sorted in the ascending order.
	exec_latencies = {}
	for msg_id, events in exec_events.items():
		exec_latencies[msg_id] = []
		# get the timestamp for the start of the execution (the mbrb-broadcast)
		for timestamp, _, log_msg in events:
			if log_msg.startswith("MBRB-broadcast"):
				start_timestamp = timestamp
				break
		# for each mbrb-delivery, compute the latency and insert it in the list
		for timestamp, _, log_msg in events:
			if log_msg.startswith("MBRB-delivered"):
				# compute the latency
				delta = timestamp - start_timestamp
				latency = delta.total_seconds()
				# find the index for inserting the latency
				idx = 0
				for inserted_latency in exec_latencies[msg_id]:
					if inserted_latency >= latency:
						break
					idx += 1
				# insert the latency in the correct position
				exec_latencies[msg_id].insert(idx, latency)
	return exec_latencies


def tuple_to_str(tp):
	return f"({','.join(map(str, tp))})"


if __name__ == "__main__":
	print("Warning! This script needs Python 3.11 or above")
	print("Current Python version:", sys.version)
	
	# get experiment timestamp
	if len(sys.argv) <= 1:
		print("Wrong number of arguments.")
		print("Usage: python expe_mbrb_step_2_analysis.py <expe timestamp>")
		sys.exit()
	timestamp = sys.argv[1]

	local_expe_folder_path = "expe"
	log_folder_path = f"{local_expe_folder_path}/logs/expe-{timestamp}"
	latencies_data_file_path = f"{local_expe_folder_path}/results/latencies_data-{timestamp}.json"
	
	logs_paths = get_logs_paths(log_folder_path)
	exec_events = get_exec_events_from_logs(logs_paths)
	exec_latencies = compute_exec_latencies(exec_events)

	latencies_data = {
		"n": get_n(timestamp, local_expe_folder_path),
		"execLatencies": {tuple_to_str(msg_id): latencies for msg_id, latencies in exec_latencies.items()}
	}
	with open(latencies_data_file_path, "w") as f:
		f.write(json.dumps(latencies_data, separators=(",", ":")))

