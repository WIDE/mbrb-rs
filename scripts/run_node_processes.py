#/bin/python.exe

import json
import sys
import subprocess
from pathlib import Path
import shutil
import re


def run_node_processes(global_config_file_path, run_config_file_path, node_proc_ids):
	for proc_id in node_proc_ids:
		cmd = f"cargo run -- expe {global_config_file_path} {run_config_file_path} {proc_id} &"
		subprocess.run(cmd, shell=True)


def get_global_config_file_path(name):
	return f"expe/config/global/global_config-{name}.json"


def get_run_config_file_path(timestamp, is_local):
	if is_local:
		return f"expe/config/run/local/run_config-{timestamp}.json"
	else:
		return f"expe/config/run/g5k/run_config-{timestamp}.json"


def read_node_config_info(node_config_file_path):
	# read node config
	with open(node_config_file_path, "r") as f:
		node_config = json.load(f)
	# return node config info
	timestamp = node_config["runTimestamp"]
	addr = node_config["addr"]
	return timestamp, addr


def read_run_config_info(run_config_file_path, addr):
	# read node config
	with open(run_config_file_path, "r") as f:
		run_config = json.load(f)
	# return run config info
	global_config_name = run_config["globalConfig"]
	node_proc_ids = []
	for process in run_config["processes"]:
		if process["addr"] == addr:
			node_proc_ids.append(process["procId"])
	return global_config_name, node_proc_ids


def recreate_log_folder(log_folder_path):
	shutil.rmtree(log_folder_path, ignore_errors=True)
	Path(log_folder_path).mkdir(parents=True, exist_ok=False)


def json_minify(struct):
	return json.dumps(struct, separators=(",", ":"))


if __name__ == "__main__":
	# get node config path
	if len(sys.argv) <= 1:
		print("Wrong number of arguments.")
		print("Usage: python run_processes.py <node config path>")
		sys.exit()
	node_config_file_path = sys.argv[1]
	is_local = bool(re.search("/local/", node_config_file_path))
	# read node config info
	timestamp, addr = read_node_config_info(node_config_file_path)
	# read run config info
	run_config_file_path = get_run_config_file_path(timestamp, is_local)
	global_config_name, node_proc_ids = read_run_config_info(run_config_file_path, addr)
	# get global config path
	global_config_file_path = get_global_config_file_path(global_config_name)
	# run node processes
	run_node_processes(global_config_file_path, run_config_file_path, node_proc_ids)
