#/bin/python.exe

import json

proc_ids_str = ", ".join([str(i+1) for i in range(100)])
print(proc_ids_str)
# for i in range(1, 100):
# 	proc_id = i+1
# 	proc_ids_str += f"{proc_id}"
# 	# proc_config = {
# 	# 	"procId": proc_id
# 	# }
# 	# proc_config_file_path = f"expe/config/proc/local/proc_config-p_{proc_id}.json"
# 	# with open(proc_config_file_path, "w") as f:
# 	# 	json.dump(proc_config, f, indent=4)

from datetime import datetime
# get node config path
if len(sys.argv) <= 2:
	print("Wrong number of arguments.")
	print("Usage: python run_processes.py <run timestamp> <city>")
	sys.exit()
timestamp = sys.argv[1]
city = sys.argv[2]
dt = datetime.fromisoformat(timestamp)
is_local = dt.year <= 2010
node_config_file_path = f"expe/config/node/{'local' if is_local else 'g5k'}/node_config-{timestamp}-{city}.json"
