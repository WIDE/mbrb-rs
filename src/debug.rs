use core::time;
use std::{
    collections::{HashSet, HashMap},
    net::{SocketAddr, ToSocketAddrs}, thread
};

use storage::{keys_storage::KeyStorage};
use crypto::{impl_bls::{BLSKeyGen, BLSMultiSigHandler}, iface::{SecKey, MultiSigHandler}};
use net::connection::{iface::Connection, impl_udp::UDPConnection};

pub fn debug() {
	let sec_keys_folder = "expe/keys/sec";
	let pub_keys_folder = "expe/keys/pub";
	let localhost = String::from("localhost");
	let processes = vec![
		ProcessInfo { proc_id: 1, addr: localhost.clone(), port: 8081 },
		ProcessInfo { proc_id: 2, addr: localhost.clone(), port: 8082 },
		ProcessInfo { proc_id: 3, addr: localhost.clone(), port: 8083 },
		ProcessInfo { proc_id: 4, addr: localhost.clone(), port: 8084 },
		ProcessInfo { proc_id: 5, addr: localhost.clone(), port: 8085 },
		ProcessInfo { proc_id: 6, addr: localhost.clone(), port: 8086 },
		ProcessInfo { proc_id: 7, addr: localhost.clone(), port: 8087 },
		ProcessInfo { proc_id: 8, addr: localhost.clone(), port: 8088 },
		ProcessInfo { proc_id: 9, addr: localhost.clone(), port: 8089 },
		ProcessInfo { proc_id: 10, addr: localhost.clone(), port: 8090 }
	];

	// CREATE IMPORTANT VARIABLES
	let multisig_handler = BLSMultiSigHandler;
	let key_storage = KeyStorage::new(sec_keys_folder, pub_keys_folder, BLSKeyGen);

	// CREATE PROCESSES MAP
	let processes_map: HashMap<u16, SocketAddr> = processes.into_iter().map(
		|member| (member.proc_id, format!("{}:{}", member.addr, member.port).as_str().to_socket_addrs().unwrap().next().unwrap())
	).collect();
	let proc_ids_set: HashSet<u16> = processes_map.keys().cloned().collect();

	// POTENTIALLY RECREATE KEY PAIRS
	let recreate_keys = true;
	if recreate_keys {
		let n = proc_ids_set.len() as u16;
		key_storage.create_key_pairs(n);
	}

	// READ KEY PAIRS FROM FILES
	let pub_keys = key_storage.read_pub_keys(&proc_ids_set);

	let pub_key1 = pub_keys.get(&1).unwrap().clone();
	let pub_key2 = pub_keys.get(&2).unwrap().clone();
	let pub_key3 = pub_keys.get(&3).unwrap().clone();
	let pub_key4 = pub_keys.get(&4).unwrap().clone();

	let sec_key1 = key_storage.read_sec_key(1);
	let sec_key2 = key_storage.read_sec_key(2);
	let sec_key3 = key_storage.read_sec_key(3);
	let sec_key4 = key_storage.read_sec_key(4);

	// SIGN MESSAGES
	/* *
	let msg1 = "message1".as_bytes();
	let msg2 = "message2".as_bytes();
	let msg3 = "message3".as_bytes();
	let msg4 = "message4".as_bytes();
	/ */
	let msg = "message bonjour".as_bytes();

	let sig1 = sec_key1.sign(msg);
	let sig2 = sec_key2.sign(msg);
	let sig3 = sec_key3.sign(msg);
	let sig4 = sec_key4.sign(msg);

	// AGGREGATE SIGNATURES
	let sigs12 = vec![sig1.as_slice(), sig2.as_slice()];
	let sigs23 = vec![sig2.as_slice(), sig3.as_slice()];
	let sigs34 = vec![sig3.as_slice(), sig4.as_slice()];

    let multisig1 = multisig_handler.aggregate(&vec![sig1.as_slice()]).unwrap();
	let multisig12 = multisig_handler.aggregate(&sigs12).unwrap();
	let multisig23 = multisig_handler.aggregate(&sigs23).unwrap();
	let multisig34 = multisig_handler.aggregate(&sigs34).unwrap();
	let multisig1223 = multisig_handler.aggregate(&vec![multisig12.as_slice(), multisig23.as_slice()]).unwrap();
	let multisig1234 = multisig_handler.aggregate(&vec![multisig12.as_slice(), multisig34.as_slice()]).unwrap();
	
	// VERIFY MULTISIGNATURES
	/*
	let msgs12 = vec![msg1, msg2];
	let msgs23 = vec![msg2, msg3];
	let msgs123 = vec![msg1, msg2, msg3];
	let msgs1223 = vec![msg1, msg2, msg2, msg3];
	let msgs1234 = vec![msg1, msg2, msg3, msg4];
	let msgs4132 = vec![msg4, msg1, msg3, msg2];
	// */

	let pub_keys12 = vec![pub_key1.clone(), pub_key2.clone()];
	let pub_keys23 = vec![pub_key2.clone(), pub_key3.clone()];
	let pub_keys123 = vec![pub_key1.clone(), pub_key2.clone(), pub_key3.clone()];
	let pub_keys1223 = vec![pub_key1.clone(), pub_key2.clone(), pub_key2.clone(), pub_key3.clone()];
	let pub_keys1234 = vec![pub_key1.clone(), pub_key2.clone(), pub_key3.clone(), pub_key4.clone()];
	let pub_keys4132 = vec![pub_key4.clone(), pub_key1.clone(), pub_key3.clone(), pub_key2.clone()];

    let valid1 = multisig_handler.verify(&vec![pub_key1.clone()], msg, &sig1);
    let valid1agg = multisig_handler.verify(&vec![pub_key1.clone()], msg, &multisig1);
	let valid12 = multisig_handler.verify(&pub_keys12, msg, &multisig12);
	let valid23 = multisig_handler.verify(&pub_keys23, msg, &multisig23);
	let valid123 = multisig_handler.verify(&pub_keys123, msg, &multisig1223);
	let valid1223 = multisig_handler.verify(&pub_keys1223, msg, &multisig1223);
	let valid1234 = multisig_handler.verify(&pub_keys1234, msg, &multisig1234);
	let valid4132 = multisig_handler.verify(&pub_keys4132, msg, &multisig1234);

	println!("keys:\tsigs:\t");
    println!("1\t1\t{}", valid1);
    println!("1\t1 (agg)\t{}", valid1agg);
	println!("12\t12\t{}", valid12);
	println!("23\t23\t{}", valid23);
	println!("123\t1223\t{}", valid123);
	println!("1223\t1223\t{}", valid1223);
	println!("1234\t1234\t{}", valid1234);
	println!("4132\t1234\t{}", valid4132);

	let addr1 = processes_map.get(&1).unwrap().clone();
	let addr2 = processes_map.get(&2).unwrap().clone();
	let conn1 = UDPConnection::new(addr1);
	let conn2 = UDPConnection::new(addr2);
	conn2.start_listening();
	conn1.send(addr2, b"hello world");

	let dur = time::Duration::from_secs(1);
	thread::sleep(dur);
}

// fn payload_from_bytes(bytes: &[u8]) -> Payload {
//     let msg = Arc::new(bytes[..MBRB_MSG_N_BYTES].to_vec());
//     let sn = bytes[MBRB_MSG_N_BYTES];
//     let sender_id = two_u8_to_u16(bytes[MBRB_MSG_N_BYTES+1], bytes[MBRB_MSG_N_BYTES+2]);
//     Payload { msg, sn, sender_id }
// }

struct ProcessInfo {
	pub proc_id: u16,
	pub addr: String,
	pub port: u16
}