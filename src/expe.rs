use std::{
	collections::HashMap,
	net::{SocketAddr, ToSocketAddrs},
	sync::{Arc, Mutex},
	path::PathBuf,
	fs
};

use log::{info, LevelFilter};
use serde::Deserialize;
use log4rs::{append::file::FileAppender, encode::pattern::PatternEncoder, Config, config::{Appender, Root}};

use mbrb::{impl_sig_based::impl_multisig_based::MultiSigBasedMBRB, iface::MBRB};
use crypto::impl_bls::{BLSKeyGen, BLSMultiSigHandler};
use net::{
	connection::{iface::Connection, impl_tcp::TCPConnection, impl_udp::UDPConnection},
	network::Network,
	msg_adversary::nothing::Nothing,
};
use storage::keys_storage::KeyStorage;

use crate::{SEC_KEYS_DIR_PATH, PUB_KEYS_DIR_PATH};

pub fn start_process(global_config_file_path: &str, run_config_file_path: &str, proc_id: u16) {
	let global_config_json = fs::read_to_string(global_config_file_path).unwrap();
	let global_config: GlobalConfig = serde_json::from_str(&global_config_json).unwrap();
	
	let run_config_json = fs::read_to_string(run_config_file_path).unwrap();
	let run_config: RunConfig = serde_json::from_str(&run_config_json).unwrap();

	let log_dir_path = format!("{}/expe-{}", run_config.parent_log_dir_path, run_config.timestamp);
	fs::create_dir_all(&log_dir_path).unwrap();

	init_log(
		&global_config.name, &run_config.timestamp,
		proc_id, &log_dir_path
	);

	let members_map: HashMap<u16, SocketAddr> = run_config.processes.into_iter().map(
		|member| (member.proc_id, format!("{}:{}", member.addr, member.port).as_str().to_socket_addrs().unwrap().next().unwrap())
	).collect();

	let key_storage = KeyStorage::new(SEC_KEYS_DIR_PATH, PUB_KEYS_DIR_PATH, BLSKeyGen);
	let pub_keys = key_storage.read_pub_keys(&members_map.keys().cloned().collect());
	let priv_key = key_storage.read_sec_key(proc_id);

	let conn: Box<dyn Connection> = match global_config.transport {
		TransportProtocol::UDP => Box::new(UDPConnection::new(members_map.get(&proc_id).unwrap().clone())),
		TransportProtocol::TCP => Box::new(TCPConnection::new(members_map.get(&proc_id).unwrap().clone()))
	};

	let strat = Nothing;
	let network = Network::new(
		conn, members_map.values().cloned().collect(), global_config.d, strat
	);
	
	let multisig_handler = BLSMultiSigHandler;
	
	let deliver_handler = move |msg: Vec<u8>, sn: u8, sender_id: u16| {
		match std::str::from_utf8(&msg) {
			Ok(msg_str) => {
				let msg_str = msg_str.trim_matches(char::from(0));
				println!("Process {proc_id}: mbrb-deliver '{msg_str}' with sequence number {sn} from process {sender_id}.");
				info!("MBRB-delivered '{msg_str}' with sequence number {sn} from process {sender_id}");
			},
			Err(_) => {
				println!("Process {proc_id}: mbrb-deliver unreadable message with sequence number {sn} from process {sender_id}.");
				info!("MBRB-delivered unreadable with sequence number {sn} from process {sender_id}");
			}
		};
	};
	
	let mbrb = MultiSigBasedMBRB::new_mutex(
		network, global_config.t, Arc::new(deliver_handler), proc_id, priv_key, pub_keys.clone(), multisig_handler
	);
	println!("Process {proc_id}: MBRB instance launched.");

	if let Some(actions) = global_config.actions.get("before") {
		for action in actions {
			execute_action(action, &mbrb, proc_id);
		}
	}
	if let Some(actions) = global_config.actions.get(&proc_id.to_string()) {
		for action in actions {
			execute_action(action, &mbrb, proc_id);
		}
	}
	if let Some(actions) = global_config.actions.get("after") {
		for action in actions {
			execute_action(action, &mbrb, proc_id);
		}
	}
	
	std::process::exit(0);
}

fn execute_action<B: MBRB>(action: &Vec<String>, mbrb: &Arc<Mutex<B>>, proc_id: u16) {
	match action[0].as_str() {
		"mbrb" => {
			let msg = &action[1];
			let sn: u8 = action[2].parse().unwrap();
			println!("Process {proc_id}: mbrb-broadcast '{msg}' with sequence number {sn}.");
			mbrb.lock().unwrap().mbrb_broadcast(msg.as_bytes(), sn).unwrap();
		},
		"wait" => {
			let dur_sec = action[1].parse().unwrap();
			let dur = std::time::Duration::from_secs(dur_sec);
			println!("Process {proc_id}: wait {dur_sec} seconds.");
			std::thread::sleep(dur);
		},
		_ => {}
	}
}

fn init_log(global_config_name: &str, run_config_timestamp: &str, proc_id: u16, log_dir_path: &str) {
	let log_file_name = format!("{}--{}--p_{}.log", global_config_name, run_config_timestamp, proc_id);
	let mut log_path = PathBuf::from(log_dir_path);
	log_path.push(log_file_name);

	std::fs::File::create(&log_path).unwrap().set_len(0).unwrap();

	//let pattern = "{h({d(%Y-%m-%dT%H:%M:%S.%f%z)} | {l} | {m}{n})}";
	let pattern = "{h({d} | {l} | {m}{n})}";
	let log_file = FileAppender::builder()
		.encoder(Box::new(PatternEncoder::new(pattern)))
		.build(&log_path).unwrap();

	let config = Config::builder()
		.appender(Appender::builder().build("logfile", Box::new(log_file)))
		.build(Root::builder().appender("logfile").build(LevelFilter::Info))
		.unwrap();

	log4rs::init_config(config).unwrap();
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct GlobalConfig {
	pub name: String,
	pub transport: TransportProtocol,
	pub t: u16,
	pub d: u16,
	//pub used_cities: Vec<String>,
	pub actions: HashMap<String, Vec<Vec<String>>>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct RunConfig {
	pub timestamp: String,
	//pub global_config: String,
	pub parent_log_dir_path: String,
	pub processes: Vec<ProcessInfo>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct ProcessInfo {
	pub proc_id: u16,
	pub addr: String,
	pub port: u16
}

#[derive(Deserialize)]
#[serde(rename_all = "lowercase")]
enum TransportProtocol { UDP, TCP }