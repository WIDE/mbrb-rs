use std::env;

use crypto::impl_bls::BLSKeyGen;
use storage::keys_storage::KeyStorage;

mod expe;
mod debug;

const PUB_KEYS_DIR_PATH: &str = "expe/keys/pub";
const SEC_KEYS_DIR_PATH: &str = "expe/keys/sec";

fn main() {
	let args: Vec<String> = env::args().collect();

	if args.len() <= 1 {
		println!("Incorrect number of arguments.\nUsage:");
		print_usage();
		return;
	}

	match args[1].as_str() {
		"expe" => {
			if args.len() != 5 {
				println!("Incorrect number of arguments.\nUsage:");
				print_expe_usage();
				return;
			}
			let proc_id: u16 = args[4].parse().unwrap();
			expe::start_process(&args[2], &args[3], proc_id);
		},
		"debug" => debug::debug(),
		"keygen" => {
			if args.len() != 3 {
				println!("Incorrect number of arguments.\nUsage:");
				print_keygen_usage();
				return;
			}
			let n: u16 = args[2].parse().unwrap();
			let key_storage = KeyStorage::new(SEC_KEYS_DIR_PATH, PUB_KEYS_DIR_PATH, BLSKeyGen);
			key_storage.create_key_pairs(n);
		},
		_ => panic!("Invalid value for the argument 'mode'. Expects one of the following: 'expe', 'debug', 'keygen'.")
	};
}

fn print_usage() {
	print_expe_usage();
	print_keygen_usage();
	print_debug_usage();
}

fn print_expe_usage() {
	// MODE EXPE: launch a process for an experiment
	// global config path: path of the global config file, which contains
	// {
	//   name: configuration name
	//   n: total number of processes,
	//   t: maximum number of Byzantine processes
	//   d: maximum number of messages that can be suppressed at each ur-bcast
	//   transport: transport protocol used (UDP|TCP)
	//   actions: {
	//	   ("before" | "after" | procId) =>
	//       ["mbrb", msg, sn] (the process 'procId' must mbrb-broadcast 'msg' with sequence number 'sn') |
	//       ["wait", dur_sec] (the process 'procId' must wait 'dur_sec' seconds)
	//   }
	// }
	// run config path: path of the run config file, which contains
	// {
	//   timestamp: timestamp (ISO 8601) of the current run of the experiment
	//   runUuid: UUID identifying the current run of the experiment
	//   parent_log_dir_path: path of the directory where logs are stored
	//   processes: [{
	//     procId: identifier of the process
	//     addr: IP address of the process
	//     port: transport layer port of the process
	//   }, ...]
	// }
	// process id: the identifier of the process to instantiate
	println!("> cargo run -- expe <global config path> <run config path> <process id>");
}

fn print_keygen_usage() {
	// MODE KEYGEN: generate key pairs for processes
	// n processes: number of processes for which to generate a key pair
	println!("> cargo run -- keygen <n processes>");
}

fn print_debug_usage() {
	// MODE DEBUG: run arbitrary code to debug the codebase
	println!("> cargo run -- debug");
}