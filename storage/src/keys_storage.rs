use std::{
	fs,
	collections::{HashSet, HashMap},
	path::{Path, PathBuf},
	io::Write,
	marker::PhantomData
};

use hex;

use crypto::iface::*;

pub struct KeyStorage<PT, S, P, KG>
	where PT: AsRef<Path>, S: SecKey, P: PubKey, KG: KeyGen<S, P>
{
	sec_keys_dir_path: PT,
	pub_keys_dir_path: PT,
	keygen: KG,
	_x: PhantomData<(S, P)>
}

impl<PT, S, P, KG> KeyStorage<PT, S, P, KG>
	where PT: AsRef<Path>, S: SecKey, P: PubKey, KG: KeyGen<S, P>
{
	pub fn new(sec_keys_dir_path: PT, pub_keys_dir_path: PT, keygen: KG) -> KeyStorage<PT, S, P, KG> {
		KeyStorage {
			sec_keys_dir_path,
			pub_keys_dir_path,
			keygen,
			_x: PhantomData
		}
	}

	pub fn create_key_pairs(&self, n: u16) {
		if fs::metadata(&self.sec_keys_dir_path).is_ok() {
			fs::remove_dir_all(&self.sec_keys_dir_path).unwrap();
		}
		if fs::metadata(&self.pub_keys_dir_path).is_ok() {
			fs::remove_dir_all(&self.pub_keys_dir_path).unwrap();
		}
		fs::create_dir(&self.sec_keys_dir_path).unwrap();
		fs::create_dir(&self.pub_keys_dir_path).unwrap();
		for proc_id in 1..n+1 {
			let (priv_key, pub_key) = self.keygen.generate();
			self.write_key(proc_id, priv_key, false);
			self.write_key(proc_id, pub_key, true);
		}
	}

	fn get_key_path(&self, proc_id: u16, is_pub: bool) -> PathBuf {
		let dir_path = if is_pub { &self.pub_keys_dir_path } else { &self.sec_keys_dir_path };
		let mut key_path = dir_path.as_ref().to_path_buf();
		key_path.push(&proc_id.to_string());
		key_path.set_extension("txt");
		key_path
	}

	fn write_key<K: Key>(&self, proc_id: u16, key: K, is_pub: bool) {
		let key_hex = bytes_to_hex(key.to_bytes().as_slice());
		let key_path = self.get_key_path(proc_id, is_pub);
		let mut key_file = fs::File::create(&key_path).unwrap();
		key_file.write_all(key_hex.as_bytes()).unwrap();
	}

	pub fn read_sec_key(&self, proc_id: u16) -> S {
		let key_path = self.get_key_path(proc_id, false);
		let key_b = hex_to_bytes(fs::read_to_string(key_path).unwrap());
		self.keygen.sec_key_from_bytes(key_b.as_slice())
	}

	pub fn read_pub_keys(&self, proc_ids: &HashSet<u16>) -> HashMap<u16, P> {
		let mut pub_keys_map = HashMap::new();
		for &proc_id in proc_ids {
			let key_path = self.get_key_path(proc_id, true);
			let key_b = hex_to_bytes(fs::read_to_string(key_path).unwrap());
			let key = self.keygen.pub_key_from_bytes(key_b.as_slice());
			pub_keys_map.insert(proc_id, key);
		}
		pub_keys_map
	}
}

pub fn bytes_to_hex(b: &[u8]) -> String {
	hex::encode(b)
}

pub fn hex_to_bytes(h: String) -> Vec<u8> {
	hex::decode(h).unwrap()
}
